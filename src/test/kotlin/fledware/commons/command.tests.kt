package fledware.commons

import kotlinx.coroutines.experimental.runBlocking
import org.junit.Assert
import org.junit.Test

class CommandActionTest {
  private fun buildSimpleAction(results: ArrayList<Int>): CommandAction {
    val builder = CommandActionBuilder("test")
    builder.registerSuspend(-2, "-2") { results.add(-2) }
    builder.registerSuspend(-1, "-1") { results.add(-1) }
    builder.registerSuspend(0, "0") { results.add(0) }
    builder.registerSuspend(1, "1") { results.add(1) }
    builder.registerSuspend(2, "2") { results.add(2) }
    return builder.build()
  }
  
  @Test
  fun testSimpleRun() {
    val results = ArrayList<Int>()
    val action = buildSimpleAction(results)
  
    Assert.assertEquals("test", action.name)
    runBlocking { action.execute() }
    
    Assert.assertEquals(5, results.size)
    Assert.assertEquals(-2, results[0])
    Assert.assertEquals(-1, results[1])
    Assert.assertEquals(0, results[2])
    Assert.assertEquals(1, results[3])
    Assert.assertEquals(2, results[4])
  }
  
  @Test
  fun testExceptionRun() = runBlocking {
    val results = ArrayList<Int>()
    val builder = CommandActionBuilder("test")
    builder.registerSuspend(-1, "-1") { results.add(-1) }
    builder.registerSuspend(0, "0") { throw Exception("lala") }
    builder.registerSuspend(1, "1") { results.add(1) }
    val action = builder.build()
  
    try {
      action.execute()
      Assert.fail()
    }
    catch (ex: CommandActionException) {
      val cause = ex.cause as Exception
      Assert.assertNotNull(cause)
      Assert.assertEquals("lala", cause.message)
      Assert.assertEquals(1, results.size)
      Assert.assertEquals(-1, results[0])
    }
  }
  
  @Test
  fun testSubRun() {
    val results = ArrayList<Int>()
    val action = buildSimpleAction(results)
  
    runBlocking { action.executeSub(start = -1, end = 1) }
  
    Assert.assertEquals(3, results.size)
    Assert.assertEquals(-1, results[0])
    Assert.assertEquals(0, results[1])
    Assert.assertEquals(1, results[2])
  }
  
  @Test
  fun testOutOfRange(): Unit = runBlocking {
    val results = ArrayList<Int>()
    val action = buildSimpleAction(results)
  
    try {
      action.executeSub(start = 5)
      Assert.fail()
    }
    catch (ex: CommandActionException) {
      Assert.assertEquals("no steps", ex.message)
    }
  }
}
