package fledware.commons

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import java.util.Properties

private enum class TestEnum {
  Foo,
  Bar
}

private val testStringVal = ConfigKey.StringVal("testStringVal")
private val testStringArrayVal = ConfigKey.StringArrayVal("testStringArrayVal")
private val testBooleanVal = ConfigKey.BooleanVal("testBooleanVal")
private val testDoubleVal = ConfigKey.DoubleVal("testDoubleVal")
private val testIntVal = ConfigKey.IntVal("testIntVal")
private val testLongVal = ConfigKey.LongVal("testLongVal")
private val testStringEnumVal = ConfigKey.StringEnumVal("testStringEnumVal", TestEnum::class)
private val testNonStringVal = ConfigKey.StringVal("testNonStringVal", default = "ok")
private val testNonStringArrayVal = ConfigKey.StringArrayVal("testNonStringArrayVal", default = listOf("food", "cats"))
private val testNonBooleanVal = ConfigKey.BooleanVal("testNonBooleanVal", default = false)
private val testNonDoubleVal = ConfigKey.DoubleVal("testNonDoubleVal", default = 1.2)
private val testNonIntVal = ConfigKey.IntVal("testNonIntVal", default = 345)
private val testNonLongVal = ConfigKey.LongVal("testNonLongVal", default = 456L)
private val testNonStringEnumVal = ConfigKey.StringEnumVal("testNonStringEnumVal", TestEnum::class, default = TestEnum.Bar)
private val testNotFound = ConfigKey.LongVal("testNotFoundAtAll")

private fun factoryConfiguration(): Configuration {
  val props = Properties()
  props.setProperty("testStringVal", "foo")
  props.setProperty("testStringArrayVal", "foo,bar")
  props.setProperty("testBooleanVal", "true")
  props.setProperty("testDoubleVal", "1.23")
  props.setProperty("testIntVal", "123")
  props.setProperty("testLongVal", "234")
  props.setProperty("testStringEnumVal", "Foo")
  return Configuration(props)
}

@RunWith(Parameterized::class)
class ConfigTest {
  companion object {
    @JvmStatic
    @Parameterized.Parameters
    fun data(): Array<Array<Any?>> {
      return arrayOf<Array<Any?>>(
          arrayOf(testStringVal, "foo"),
          arrayOf(testStringArrayVal, listOf("foo", "bar")),
          arrayOf(testBooleanVal, true),
          arrayOf(testDoubleVal, 1.23),
          arrayOf(testIntVal, 123),
          arrayOf(testLongVal, 234L),
          arrayOf(testStringEnumVal, TestEnum.Foo),
          arrayOf(testNonStringVal, "ok"),
          arrayOf(testNonStringArrayVal, listOf("food", "cats")),
          arrayOf(testNonBooleanVal, false),
          arrayOf(testNonDoubleVal, 1.2),
          arrayOf(testNonIntVal, 345),
          arrayOf(testNonLongVal, 456L),
          arrayOf(testNonStringEnumVal, TestEnum.Bar),
          arrayOf(testNotFound, ConfigurationMissingException(testNotFound.name))
      )
    }
  }
  
  private val config = factoryConfiguration()
  private val key: ConfigKey<*>
  private val result: Any?
  
  constructor(key: ConfigKey<*>, result: Any?) {
    this.key = key
    this.result = result
  }
  
  @Test
  fun testSimpleGet() {
    println("$key -> $result")
    (0..9).forEach {
      try {
        Assert.assertEquals(result, config.get(key))
      }
      catch (ex: Exception) {
        if (result is ConfigurationMissingException &&
            ex is ConfigurationMissingException) {
          Assert.assertEquals(result.missing, ex.missing)
        }
        else {
          throw ex
        }
      }
    }
  }
}
