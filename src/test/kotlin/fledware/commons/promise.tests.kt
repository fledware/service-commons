package fledware.commons

import org.junit.Assert
import org.junit.Test
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import java.util.concurrent.atomic.AtomicBoolean

class PromiseTests {
  @Test
  fun testRunIfNotComplete() {
    val promise = Promise<Unit>()
    val check = AtomicBoolean(false)
    promise.runIfNotComplete(500) { check.set(true) }
    
    Thread.sleep(100)
    Assert.assertFalse(check.get())
    Assert.assertFalse(promise.isDone)
    
    Thread.sleep(500)
    Assert.assertTrue(check.get())
    Assert.assertFalse(promise.isDone)
  }
  
  @Test
  fun testRunIfNotCompleteDoesntRunIfComplete() {
    val promise = Promise<Unit>()
    val check = AtomicBoolean(false)
    promise.runIfNotComplete(500) { check.set(true) }
  
    Thread.sleep(100)
    Assert.assertFalse(check.get())
    Assert.assertFalse(promise.isDone)
    promise.complete(Unit)
  
    Thread.sleep(500)
    Assert.assertFalse(check.get())
    Assert.assertTrue(promise.isDone)
  }
  
  @Test
  fun testWithTimeout() {
    val promise = Promise<Unit>().withTimeout(500, "some timeout message")
    try {
      promise.get(600, TimeUnit.MILLISECONDS)
      Assert.fail()
    }
    catch (ex: ExecutionException) {
      val cause = ex.cause as TimeoutException
      Assert.assertNotNull(cause)
      Assert.assertEquals("some timeout message", cause.message)
    }
  }
  
  @Test
  fun testWithTimeoutDoesntRunIfComplete() {
    val promise = Promise<Unit>().withTimeout(500, "some timeout message")
    
    Thread.sleep(100)
    Assert.assertFalse(promise.isDone)
    promise.complete(Unit)
  
    Thread.sleep(500)
    Assert.assertFalse(promise.isCompletedExceptionally)
    Assert.assertTrue(promise.isDone)
  }
}
