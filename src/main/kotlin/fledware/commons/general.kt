package fledware.commons

fun currentTime(): Long = System.currentTimeMillis()

@Suppress("UNCHECKED_CAST")
fun getEnumValue(enumClass: Class<*>, value: String): Enum<*> {
  val enumConstants = enumClass.enumConstants as Array<out Enum<*>>
  return enumConstants.first { it.name == value }
}
