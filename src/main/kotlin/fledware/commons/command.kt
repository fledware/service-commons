package fledware.commons

import kotlinx.coroutines.experimental.future.await
import kotlinx.coroutines.experimental.future.future
import org.kodein.di.Kodein
import org.kodein.di.direct
import org.kodein.di.generic.instance
import org.slf4j.LoggerFactory
import java.util.concurrent.ConcurrentHashMap
import kotlin.system.measureTimeMillis

/**
 * exception used for this section of the code
 */
class CommandActionException(message: String, cause: Throwable? = null) : RuntimeException(message, cause)

/**
 * represents a single task
 */
internal data class ExecutionTask(val name: String,
                                  val call: () -> Promise<Unit>)

/**
 * represents all the tasks that are in a step
 */
internal data class ExecutionStep(val order: Int,
                                  val tasks: List<ExecutionTask>)

/**
 * class for registering all of the commands.
 * this lets us build things as we initiate the system
 */
class CommandRegistry {
  val commands = ConcurrentHashMap<String, CommandFactory>()
  
  fun getBuilder(name: String) =
      commands.computeIfAbsent(name, { CommandFactory(name) }).builder
  
  fun get(name: String) =
      commands.computeIfAbsent(name, { throw CommandActionException("command not found: $name") }).command
  
  fun contains(name: String) =
      commands.contains(name)
}

/**
 *
 */
fun Kodein.executeCommandPromise(name: String): Promise<Unit> =
    direct.instance<CommandRegistry>().get(name).executePromise()

/**
 * used as a helper to be able to build a
 * command until the first invocation of it,
 * then it gets locked.
 */
class CommandFactory(val name: String) {
  private var _builder: CommandActionBuilder? = CommandActionBuilder(name)
  val builder get() = _builder ?: throw CommandActionException("$name is finished building")
  
  private var _command: CommandAction? = null
  val command: CommandAction
    get() {
      if (_command == null) {
        synchronizedSetup()
      }
      return _command!!
    }
  
  @Synchronized
  private fun synchronizedSetup() {
    if (_command == null) {
      _command = builder.build()
      _builder = null
    }
  }
}

/**
 * the builder to put together the actual action
 */
class CommandActionBuilder(val name: String) {
  private val registered = HashMap<Int, MutableList<ExecutionTask>>()
  
  @Synchronized
  fun registerPromise(order: Int, name: String, hook: () -> Promise<Unit>) {
    registered.computeIfAbsent(orderCheck(order), { ArrayList() }).add(ExecutionTask(name, hook))
  }
  
  @Synchronized
  fun registerSuspend(order: Int, name: String, hook: suspend () -> Unit) {
    registered.computeIfAbsent(orderCheck(order), { ArrayList() }).add(ExecutionTask(name, { future { hook() } }))
  }
  
  private fun orderCheck(order: Int): Int {
    if (order == Int.MAX_VALUE || order == Int.MIN_VALUE)
      throw CommandActionException("cannot have Int.MAX_VALUE or Int.MIN_VALUE for order")
    return order
  }
  
  @Synchronized
  fun build(): CommandAction {
    val executing: List<ExecutionStep> = registered.entries
        .map { ExecutionStep(it.key, it.value) }
        .sortedBy { it.order }
    return CommandAction(name, executing)
  }
}

/**
 * this class is used to register and order async actions. this
 * is the workhorse for ordered executions of actions.
 */
class CommandAction {
  
  companion object {
    private val logger = LoggerFactory.getLogger(CommandAction::class.java)
  }
  
  val name: String
  private val executing: List<ExecutionStep>
  
  internal constructor(name: String, executing: List<ExecutionStep>) {
    this.name = name
    this.executing = executing
  }
  
  /**
   * execute all the steps in order
   */
  suspend fun execute() =
      executeSteps(executing)
  
  /**
   * execute all the steps in order outside of a suspend context
   */
  fun executePromise() =
      future { executeSteps(executing) }
  
  /**
   * execute some subset of the steps, still in order
   */
  suspend fun executeSub(start: Int = Int.MIN_VALUE, end: Int = Int.MAX_VALUE) =
      executeSteps(executing.filter { it.order in start..end })
  
  /**
   * execute some subset of the steps, still in order outside of a suspend context
   */
  suspend fun executeSubPromise(start: Int = Int.MIN_VALUE, end: Int = Int.MAX_VALUE) =
      future { executeSteps(executing.filter { it.order in start..end }) }
  
  private suspend fun executeSteps(steps: List<ExecutionStep>) {
    if (steps.isEmpty()) throw CommandActionException("no steps")
    logger.info("executing {} with {} steps", name, steps.size)
    try {
      val total_time = measureTimeMillis {
        steps.forEachIndexed { index, step -> executeStep(step, index + 1, steps.size) }
      }
      logger.info("finished executing {} in {}ms", name, total_time)
    }
    catch (ex: Exception) {
      logger.error("unable to perform action $name", ex)
      throw CommandActionException("exception during $name", ex)
    }
  }
  
  private suspend fun executeStep(step: ExecutionStep, step_index: Int, step_count: Int) {
    if (logger.isDebugEnabled) {
      logger.debug("executing ($step_index / $step_count): ${step.tasks.map { it.name }}")
    }
    val step_time = measureTimeMillis {
      step.tasks.map { executeTask(it, step_index, step_count) }.forEach { it.await() }
    }
    logger.debug("executing ($step_index / $step_count): step finished in $step_time ms")
  }
  
  private suspend fun executeTask(task: ExecutionTask, step_index: Int, step_count: Int) = future {
    val call_time = measureTimeMillis {
      task.call().await()
    }
    logger.debug("executing ($step_index / $step_count): ${task.name} finished in $call_time ms")
  }
}
