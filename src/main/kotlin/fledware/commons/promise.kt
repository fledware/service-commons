package fledware.commons

import kotlinx.coroutines.experimental.CancellationException
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory
import java.util.concurrent.CompletableFuture

private val logger = LoggerFactory.getLogger("queue.util.promise")

typealias Promise<T> = CompletableFuture<T>
private val COMPLETED = Promise.completedFuture<Unit>(null)

@Suppress("UNCHECKED_CAST")
fun <T> promiseNullComplete(): Promise<T> = COMPLETED as Promise<T>

@Suppress("UNCHECKED_CAST")
fun promiseUnitComplete(): Promise<Unit> = COMPLETED as Promise<Unit>

fun <T> promiseFailed(th: Throwable): Promise<T> = Promise<T>().also { it.completeExceptionally(th) }

fun <T> Promise<T>.whenException(block: (Throwable) -> Unit): Promise<T> = whenComplete { _, th -> if (th != null) block(th) }

fun <T> Promise<T>.proxyFinished(promise: Promise<T>): Promise<T> {
  this.whenComplete { result, th ->
    when (th) {
      null -> promise.complete(result)
      else -> promise.completeExceptionally(th)
    }
  }
  return promise
}

fun <T> Promise<T>.runIfNotComplete(timeout_ms: Long, block: (Promise<T>) -> Unit) {
  val job = launch {
    try {
      delay(timeout_ms)
      if (!this@runIfNotComplete.isDone) block(this@runIfNotComplete)
    }
    catch (ex: CancellationException) {
      // nothing to do..
    }
    catch (ex: Exception) {
      logger.error("error while runIfNotComplete", ex)
    }
  }
  this.whenComplete { _, _ -> job.cancel() }
}

fun <T> Promise<T>.withTimeout(timeout_ms: Long, message: String = ""): Promise<T> {
  this.runIfNotComplete(timeout_ms) { this.completeExceptionally(java.util.concurrent.TimeoutException(message)) }
  return this
}
