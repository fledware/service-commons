## getting

add this to build.gradle
```$xslt
  repositories {
    ...
    maven {
      url "https://dl.bintray.com/rexfleischer/fledware"
    }
  }
  
  dependencies {
    ...
    compile group: 'fledware', name: 'service-commons', version: '0.0.2'
  }
```